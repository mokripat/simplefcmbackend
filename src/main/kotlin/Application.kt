package com.example

import com.google.auth.oauth2.GoogleCredentials
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseOptions
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.messaging.Message
import com.google.firebase.messaging.Notification
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.response.*
import io.ktor.routing.*
import java.io.FileInputStream

val tokenDatabase = HashMap<String, String>()

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

fun Application.module(testing: Boolean = false) {
    var messageIdIndex: Int = 0
    val imageUrl =
        "https://www.google.com/url?sa=i&url=https%3A%2F%2Fen.wikipedia.org%2Fwiki%2FFacebook_like_button&psig=AOvVaw0dRZk7Ts3Ng5s4hfv6YkRk&ust=1629731885460000&source=images&cd=vfe&ved=0CAsQjRxqFwoTCOj95MP2xPICFQAAAAAdAAAAABAD"

    val serviceAccount =
        FileInputStream("/Users/patrik.mokris/IdeaProjects/notifBackend/src/main/resources/dpl-fcm-demo-firebase-adminsdk-ed3hy-50633e372f.json")

    val options: FirebaseOptions = FirebaseOptions.builder()
        .setCredentials(GoogleCredentials.fromStream(serviceAccount))
        .build()

    FirebaseApp.initializeApp(options)

    routing {
        get("/") {
            call.respondText("Hello, world!")
            log.debug("Hello World called")
        }

        post("/register/{userId}/{fcmToken}") {
            val userId: String = call.parameters["userId"]!!
            val token: String = call.parameters["fcmToken"]!!

            tokenDatabase[userId] = token
            log.debug("New token $token registered for user with id $userId")

            call.respond(HttpStatusCode.OK)
        }

        get("/{severity}/{userId}/{messageToSend}") {
            val severity: String = call.parameters["severity"]!!
            val userId: String = call.parameters["userId"]!!
            val messageToSend: String? = call.parameters["messageToSend"]!!
            val recipientToken = tokenDatabase[userId]

            log.debug("send notification service called, args: $messageIdIndex: $messageToSend")

            val fcmMessage: Message = Message.builder()
                .putData("id", messageIdIndex.toString())
                .putData("severity", severity)
                .putData("message", messageToSend)
                .setToken(recipientToken)
                .build()

            val response = FirebaseMessaging.getInstance().send(fcmMessage)

            if (response != null) {
                call.respond(HttpStatusCode.OK)
                messageIdIndex++
                log.debug("send notification service successful")
            } else {
                call.respond(HttpStatusCode.BadRequest, "firebase is on fire")
                log.debug("send notification service failed")
            }
        }
    }
}